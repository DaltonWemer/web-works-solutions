# Setup

I used Node.js to run and test all of the scripts in this repositiory

## Installation

To install node, head over to https://nodejs.org/en/download/ and choose your platform.

## To Run

Once you have pulled down this repo, cd into the dirctory and run

```bash
node msuBears.js  
```
```bash
node fileOperation.js  
```
```bash
node fibonacciSequence.js  
```
