// Dalton Wemer

// Imports
var fs = require('fs');

// Reading a file in asynchronously
try {
    var data = fs.readFileSync('demo.txt', 'utf8');

    // Split the data into lines
    let newData = data.split("\n"[0])

    // Check only the first line for its inclusion of the word bear
    if(newData[0].includes('bear')){
   		console.log('the word bear is in the first line of the file')
  	}else{
  		console.log('the word bear is not in the first line of the file')
  	}
} catch(e) {
    console.log('Error:', e.stack);
}