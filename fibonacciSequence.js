// Dalton Wemer

// This was a problem we had to solve in Computational Thinking course when going over
// recursion in python
let i = 0;

function fib(n) {
  // The base case
  if (n < 2){  	
    return n
  }
  i++
  return fib(n - 1) + fib (n - 2)
}

// Call the function
fib(5)
// everytime we call the fib function we are adding to our i variable
// we add one at the end to account for the base case
console.log(i + 1)